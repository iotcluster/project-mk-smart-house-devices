#include "IoTSensorHX711.h"
#include <IoTCore.h>
#include <IoTSensorDHT22.h>
#include <IoTSensorTSL2561.h>

IoTCore iotCore;
IoTSensorDHT22* sensorHumidity = new IoTSensorDHT22(D7, DHT22_TYPE_HUMIDITY, DHT22_TYPE_HUMIDITY);
IoTSensorDHT22* sensorTemperature = new IoTSensorDHT22(D7, DHT22_TYPE_TEMPERATURE, DHT22_TYPE_TEMPERATURE);

IoTSensorTSL2561* sensorLuxury = new IoTSensorTSL2561("Luxury1");

void setup()
{
	logger.begin();

	iotCore.getUpdater().setConfig("#type:OutDoorDHT22AndLuxury", "#version:v1");
	iotCore.getTransport().setDefaultWifiConfig("mantas", "");

	iotCore.setName("OutDoorDHT22AndLuxury");
	
	sensorTemperature->getConf()->setDefault("Temperature", "outdoor-temperature");
	sensorHumidity->getConf()->setDefault("Humidity", "outdoor-humidity");
	sensorLuxury->getConf()->setDefault("Luxury", "outdoor-luxury");

	iotCore.registerSensor(sensorTemperature);
	iotCore.registerSensor(sensorHumidity);
	iotCore.registerSensor(sensorLuxury);

	iotCore.begin();
}

void loop()
{
	iotCore.loop();
}
