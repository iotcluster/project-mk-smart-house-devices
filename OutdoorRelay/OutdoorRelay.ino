#include <IoTCore.h>
#include <IoTSensorOutput.h>

#define RELAY_COUNT 8
IoTCore iotCore;

IoTSensorOutput* relayArray[RELAY_COUNT] = {
	new IoTSensorOutput(D1, "R1"),
	new IoTSensorOutput(D2, "R2"),
	new IoTSensorOutput(D3, "R3"),
	new IoTSensorOutput(D4, "R4"),
	new IoTSensorOutput(D5, "R5"),
	new IoTSensorOutput(D6, "R6"),
	new IoTSensorOutput(D7, "R7"),
	new IoTSensorOutput(D8, "R8")
};

void setup()
{
	logger.begin();
	
	iotCore.getUpdater().setConfig("#type:8RelayModule", "#version:v4");
	iotCore.getTransport().setDefaultWifiConfig("mantas", "");

	iotCore.setName("Outdoor Relays");

	for (size_t i = 0; i < RELAY_COUNT; i++)
	{
		String sufix = String(i + 1);
		relayArray[i]->setInitOututValue(HIGH);
		relayArray[i]->setInvertValueOnSend();
		relayArray[i]->getConf()->setDefault(String("No") + "-" + sufix, String("Outdoor-Relay") + "-" + sufix);
		iotCore.registerSensor(relayArray[i]);
	}


	iotCore.begin();

}

void loop()
{
	iotCore.loop();
}
