#include <IoTCore.h>
#include <IoTSensorDHT22.h>
#include <IoTSensorBatteryLife.h>

#define DATA_PATH		"Demo"

IoTCore iotCore;
IoTSensorDHT22* sensorHumidity = new IoTSensorDHT22(D1, DHT22_TYPE_HUMIDITY, DHT22_TYPE_HUMIDITY);
IoTSensorDHT22* sensorTemperature= new IoTSensorDHT22(D1, DHT22_TYPE_TEMPERATURE, DHT22_TYPE_TEMPERATURE);
IoTSensorBatteryLife* sensorBatteryLife = new IoTSensorBatteryLife();
ADC_MODE(ADC_VCC);

void setup()
{
	logger.begin();
	iotCore.setName(DATA_PATH);
	
	iotCore.getUpdater().setConfig("#type:DHT22", "#version:v2");
	//iotCore.getTransport().setDefaultMQConfig("home.iotcluster.lt", "", "", 1883);
	iotCore.getTransport().setDefaultMQConfig("192.168.0.154", "", "", 1883);
	iotCore.getTransport().setDefaultWifiConfig("mantas", "");
	//iotCore.getTransport().setDefaultWifiConfig("TeleSoftas", "AsNedalinuSlaptazodziu%2");
	
	sensorTemperature->getConf()->setDefault("Temperature", DATA_PATH);
	sensorHumidity->getConf()->setDefault("Humidity", DATA_PATH);
	sensorBatteryLife->getConf()->setDefault("Battery", DATA_PATH);

	sensorTemperature->setReadValueInterval(5000);
	sensorHumidity->setReadValueInterval(5000);

	iotCore.registerSensor(sensorTemperature);
	iotCore.registerSensor(sensorHumidity);
	iotCore.registerSensor(sensorBatteryLife);

	iotCore.begin();

}

void loop()
{
	iotCore.loop();
}
