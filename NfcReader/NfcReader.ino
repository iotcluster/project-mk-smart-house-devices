#include <IoTSensorAbstractInterval.h>
#include <IoTCore.h>
#include <IoTSensorMFRC522.h>
#define DATA_PATH		"NFC"
IoTCore iotCore;

IoTSensorMFRC522* sensorMFRC522 = new IoTSensorMFRC522();

void setup()
{

	logger.begin();
	iotCore.setName(DATA_PATH);

	iotCore.getUpdater().setConfig("#type:NFC", "#version:v1");
	iotCore.getTransport().setDefaultMQConfig("home.iotcluster.lt", "", "", 1883);
	//iotCore.getTransport().setDefaultMQConfig("192.168.0.154", "", "", 1883);
	//iotCore.getTransport().setDefaultWifiConfig("mantas", "");
	iotCore.getTransport().setDefaultWifiConfig("TeleSoftas", "AsNedalinuSlaptazodziu%2");

	sensorMFRC522->getConf()->setDefault(DATA_PATH, DATA_PATH);

	iotCore.registerSensor(sensorMFRC522);

	iotCore.begin();
}

void loop()
{

	iotCore.loop();

}