
#include <IoTCore.h>
#include <IoTSensorSwitchWithState.h>


#define TXT_ODETOS_ROOM		"Odetos-Room"

IoTCore iotCore;
IoTSensorSwitchWithState* sensorSwitchWithState = new IoTSensorSwitchWithState(D1, D2, "1");

void setup()
{
	logger.begin();

	iotCore.getUpdater().setConfig("#type:SWITCH_WITH_STATE", "#version:v3");
	iotCore.getTransport().setDefaultWifiConfig("mantas", "");
	iotCore.setName(TXT_ODETOS_ROOM);
	sensorSwitchWithState->getConf()->setDefault(TXT_ODETOS_ROOM, TXT_ODETOS_ROOM);

	iotCore.registerSensor(sensorSwitchWithState);

	iotCore.begin();

}

void loop()
{
	iotCore.loop();
}
